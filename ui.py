# -*- coding: utf-8 -*-

import tkinter as tk
import ttk
import tkinter.filedialog as tkfd

from datetime import datetime
import time
import os.path

import reader
import extractors
import db
from utils import *

class Application(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)

        root = self.winfo_toplevel()
        root.title('Annotations extractor')
        root.minsize(400,300)

        self.grid(sticky=tk.N+tk.S+tk.E+tk.W)
        self.createWidgets()

        db_dir = os.path.join(reader.dev_install_dir, reader.db_dirname)
        if os.path.isdir(db_dir):
            self.db_dir = db_dir
        else:
            self.db_dir = None
        
        self.show_available_files()

    def createWidgets(self):
        top=self.winfo_toplevel()
        top.rowconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)

        self.quitButton = tk.Button(self, text='Quit',
                                    command=quit)
        self.quitButton.grid(row=0, column=0)

        self.extractButton = tk.Button(self, text='Extract annotations from selected meeting',
                                       command=self.extract_selected)
        self.extractButton.grid(row=0, column=1)

        self.reloadButton = tk.Button(self, text='Reload meetings list',
                                       command=self.show_available_files)
        self.reloadButton.grid(row=0, column=2)

        self.reloadButton = tk.Button(self, text='Choose MCB intall directory',
                                       command=self.choose_dir_and_reload)
        self.reloadButton.grid(row=0, column=3)

        self.files_listbox = tk.Listbox(self, activestyle='dotbox')
        self.files_listbox.bind('<<ListboxSelect>>', lambda evt : self.extract_selected())
        self.files_listbox.grid(row=1, column=0, columnspan=4,
                             sticky=tk.N+tk.S+tk.E+tk.W)

        self.annotations_view = ttk.Treeview(self, columns=('code', 'date', 'description',
                                                            'uuid', 'timestamp'),
                                             displaycolumns='#all',
                                             show = ['headings'])
        self.annotations_view.heading('code', text = 'Code')
        self.annotations_view.heading('date', text = 'Date')
        self.annotations_view.heading('description', text = 'Description')
        self.annotations_view.heading('uuid', text = 'Element UUID')
        self.annotations_view.heading('timestamp', text = 'Timestamp')

    def show_available_files(self):
        if not(self.db_dir):
            self.choose_dir()
        self.files = reader.find_history_files(self.db_dir)
        self.loaded_histories = {}

        self.files_listbox.delete(0, tk.END)
        
        items = []
        for f in self.files:
            self.loaded_histories[f] = reader.parse_history_file(f)
            topic = self.loaded_histories[f]['topic']
            items.append('({}) {}'.format(topic['id'], get_i18n_text_best(topic['topic']['content'])))

        self.files_listbox.insert(0, *items)

    def choose_dir_and_reload(self):
        self.choose_dir()
        self.show_available_files()

    def choose_dir(self):
        install_dir = tkfd.askdirectory(title='Choose the directory where MCB is installed')
        self.db_dir = os.path.join(install_dir, reader.db_dirname)

    def extract_selected(self):
        selection = self.files_listbox.curselection()
        if len(selection) == 1:
            annotations = reader.extract_annotations_from_history(self.loaded_histories[self.files[selection[0]]]['states'])

            self.display_annotations(annotations)

    def display_annotations(self, annotations):
        self.annotations_view.grid(row=2, column=0, columnspan=4,
                             sticky=tk.N+tk.S+tk.E+tk.W)
        self.annotations_view.delete(*self.annotations_view.get_children())

        for annotation in annotations:
            formated_date = datetime.utcfromtimestamp((annotation.timestamp // 1000) - time.timezone).strftime('%c')
            self.annotations_view.insert('', tk.END, values = (annotation.code, formated_date, annotation.data,
                                                               annotation.uuid, annotation.timestamp))


app = Application()
app.master.title('Sample application')
app.mainloop()
