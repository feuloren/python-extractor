# -*- coding: utf-8 -*-

def get_i18n_text_best(text: dict):
    for lang in ("ENGLISH", "FRENCH", "JAPANESE"):
        if lang in text.keys():
            return text[lang]
    raise KeyError("No language found")
